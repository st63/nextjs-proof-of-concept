import Head from 'next/head'

export default function PublicPage() {
  return (
    <div>
      <Head>
        <title>Public Page</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      Public Page
    </div>
  )
}