import Head from 'next/head'

export default function PrivatePage({ users }) {
  if (!users) return <div>Loading...</div>

  return (
    <div>
      <Head>
        <title>Private Page</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className='container'>
        <h1>Список пользователей</h1>
        <ul>
          {users.map(({ id, name }) => <li key={id}>{name}</li>)}
        </ul>
      </main>

      <style jsx>{`
        .container {
          display: flex;
          align-items: center;
          flex-direction: column;
        }
      `}</style>
    </div>
  )
}

export async function getStaticProps() {
  const request = await fetch('http://localhost:3000/api/users')
  const users = await request.json()

  return {
    props: {
      users,
    }
  }
}