import Head from 'next/head'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import Router from 'next/router';

export default function Home() {
  const [userName, setUserName] = useState(null);
  const [userAvatar, setUserAvatar] = useState(null);

  useEffect(() => {
    window.gapi.load('auth2', function () {
      window.gapi.auth2.init({
        client_id: process.env.GOOGLE_AUTH_TOKEN
      });
    });
  }, []);

  const signIn = (redirect) => {
    const authOk = (googleUser) => {
      setUserName(googleUser.getBasicProfile().getName())
      setUserAvatar(googleUser.getBasicProfile().getImageUrl())

      if (redirect) {
        Router.push('/privatepage')
      }
    }

    const GoogleAuth = window.gapi.auth2.getAuthInstance();

    GoogleAuth.signIn({
      scope: 'profile email'
    }).then(authOk, () => console.log('Auth Error'));
  }

  const signOut = () => {
    const signOutOk = () => {
      setUserName(0);
      setUserAvatar(0);
    }

    const GoogleAuth = window.gapi.auth2.getAuthInstance();

    GoogleAuth.signOut().then(signOutOk, () => console.log('Sign Out Error'))
  }

  return (
    <div className="container">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
        <script src="https://apis.google.com/js/platform.js?onload=init" async defer></script>
      </Head>

      <main>
        {userName
          ? <button onClick={signOut}>Log out</button>
          : <button onClick={() => signIn(false)}>Log in</button>}

        {userName
          ? <div className='greeting'>Привет {userName}</div>
          : <div className='please-log-in'>Пожалуйста авторизуйтесь</div>}

        {userAvatar
          ? <img className='user-avatar' src={userAvatar} />
          : null}

        {!userName
          ? <a href='#' onClick={() => signIn(true)}>Перейти на приватную страницу</a>
          : <Link href="/privatepage"><a>Перейти на приватную страницу</a></Link>}

        <Link href="/publicpage">
          <a>Перейти на публичную страницу</a>
        </Link>
      </main>

      <style jsx>{`
        .container {
          min-height: 100vh;
          padding: 0 0.5rem;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        main {
          padding: 5rem 0;
          flex: 1;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        .greeting,
        .please-log-in {
          margin: 40px 0px;
        }

        .user-avatar {
          border-radius: 50%;
          margin-bottom: 40px;
        }

      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  )
}
