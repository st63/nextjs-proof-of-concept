module.exports = {
  reactStrictMode: true,
  env: {
    GOOGLE_AUTH_TOKEN: process.env.GOOGLE_AUTH_TOKEN,
  }
}